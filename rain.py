from selenium import webdriver
import undetected_chromedriver as uc
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time
import os
import re
u = os.environ['U_RAIN']
p = os.environ['P_RAIN']
chrome_options = uc.ChromeOptions()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-gpu')
chrome_options.add_argument('--disable-dev-shm-usage')
chromedriver = "/usr/bin/chromedriver"
os.environ["webdriver.chrome.driver"] = chromedriver
driver = uc.Chrome(chrome_options=chrome_options)


driver.get('https://app.rainyun.cc/account/reward/earn')
time.sleep(5)
driver.find_element(By.XPATH,'//*[@id="field"]').send_keys(u)
driver.find_element(By.NAME,'login-password').send_keys(p)
driver.find_element(By.XPATH,'//*[@id="app"]/div[1]/div[1]/div/div[2]/div/div/span/form/button').click()
time.sleep(5)
driver.find_element(By.XPATH,'/html/body/div[2]/div[1]/div[3]/div[2]/div/div/div[2]/div[2]/div/div/div/div/div/div[1]/div/div[1]/div/span[2]/a').click()
time.sleep(2)
driver.quit()
