import undetected_chromedriver as uc
from selenium.webdriver.remote.webdriver import By
import os
from time import sleep
u = os.environ['U_HD']
p = os.environ['P_HD']
chrome_options = uc.ChromeOptions()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-gpu')
chrome_options.add_argument('--disable-dev-shm-usage')
driver = uc.Chrome(options=chrome_options)
driver.get('https://hdtime.org/login.php')
sleep(3)
driver.find_element(By.XPATH,'//*[@id="nav_block"]/form[2]/table/tbody/tr[1]/td[2]/input').send_keys(u)
driver.find_element(By.XPATH,'//*[@id="nav_block"]/form[2]/table/tbody/tr[2]/td[2]/input').send_keys(p)
driver.find_element(By.XPATH,'//*[@id="nav_block"]/form[2]/table/tbody/tr[8]/td/input[1]').click()
sleep(3)
driver.find_element(By.XPATH,'//*[@id="info_block"]/tbody/tr/td/table/tbody/tr/td[1]/span/a[5]').click()
sleep(3)
driver.get('https://hdtime.org/attendance.php')
sleep(3)
driver.quit()
