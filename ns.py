import undetected_chromedriver as uc
from selenium.webdriver.remote.webdriver import By

import os
from time import sleep
u = os.environ['U_NS']
p = os.environ['P_NS']
chrome_options = uc.ChromeOptions()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-gpu')
chrome_options.add_argument('--disable-dev-shm-usage')
driver = uc.Chrome(options=chrome_options)
driver.get('https://www.nodeseek.com/signIn.html')
driver.find_element(By.XPATH,'//*[@id="Form_Name"]').send_keys(u)
driver.find_element(By.XPATH,'//*[@id="Form_Password"]').send_keys(p)
driver.find_element(By.XPATH,'//*[@id="Form_ApplyforMembership"]').click()
sleep(3)
driver.get('https://www.nodeseek.com/board')
sleep(5)
driver.find_element(By.XPATH,'//*[@id="Frame"]/div[2]/div[1]/div/button[1]').click()
driver.quit()
